/////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Audiokinetic Wwise generated include file. Do not edit.
//
/////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __WWISE_IDS_H__
#define __WWISE_IDS_H__

#include <AK/SoundEngine/Common/AkTypes.h>

namespace AK
{
    namespace EVENTS
    {
        static const AkUniqueID CONTINUE = 3405713266U;
        static const AkUniqueID EVENTOPLAY = 1944009944U;
        static const AkUniqueID INICIOMUSICALOBBY = 3091347418U;
        static const AkUniqueID INICIOMUSICAPARTIDA = 3691514611U;
        static const AkUniqueID INICIOPARTIDA = 623918673U;
        static const AkUniqueID JUMP = 3833651337U;
        static const AkUniqueID KNOCKBACK = 2762859538U;
        static const AkUniqueID MORIR = 555663540U;
        static const AkUniqueID NORMAL_SHOT = 1547041703U;
        static const AkUniqueID OHMYGLASSES = 1241193786U;
        static const AkUniqueID PAUSE = 3092587493U;
        static const AkUniqueID PAUSE_MUSIC = 2735935537U;
        static const AkUniqueID PLAYERJOIN = 3875582670U;
        static const AkUniqueID POCOTIEMPO = 1946310644U;
        static const AkUniqueID SHOTGUN = 51683977U;
        static const AkUniqueID STOPINICIOPARTIDA = 769706777U;
        static const AkUniqueID STOPMUSICALOBBY = 833651181U;
        static const AkUniqueID STOPMUSICANORMAL = 1937486042U;
        static const AkUniqueID SUMARPUNTO = 3852334007U;
        static const AkUniqueID VOLVERMUSICA = 4135124037U;
    } // namespace EVENTS

    namespace BANKS
    {
        static const AkUniqueID INIT = 1355168291U;
        static const AkUniqueID MAIN = 3161908922U;
    } // namespace BANKS

    namespace BUSSES
    {
        static const AkUniqueID MASTER_AUDIO_BUS = 3803692087U;
    } // namespace BUSSES

    namespace AUDIO_DEVICES
    {
        static const AkUniqueID NO_OUTPUT = 2317455096U;
        static const AkUniqueID SYSTEM = 3859886410U;
    } // namespace AUDIO_DEVICES

}// namespace AK

#endif // __WWISE_IDS_H__
