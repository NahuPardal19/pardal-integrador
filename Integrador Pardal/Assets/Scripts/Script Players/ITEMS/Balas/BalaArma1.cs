using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaArma1 : MonoBehaviour
{
    private Rigidbody2D rb;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Destroy(gameObject, 5);
    }

    void Update()
    {

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<InputPruebas>().Empujarse(collision.GetContact(0).normal);
            //print(collision.GetContact(0).normal);
            Destroy(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }
}
