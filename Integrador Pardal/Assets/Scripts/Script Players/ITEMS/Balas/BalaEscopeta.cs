using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEscopeta : MonoBehaviour
{
    [SerializeField] private Vector2 zonaDeath;
    void Start()
    {
        Destroy(gameObject, 0.5f);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            print("pego");
            collision.gameObject.transform.position = zonaDeath;
        }
        Destroy(gameObject);
    }
}
