using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Jetpack : MonoBehaviour
{
    [SerializeField] private int NumeroJugador;

    private bool EstaVolando;
    [SerializeField] private float NaftaMax;
    [SerializeField] private float FuerzaJetpack;
    [SerializeField] private Transform pruebaPiso;
    [SerializeField] private ParticleSystem fire;
    [SerializeField] private Rigidbody2D rb;
    [SerializeField] private LayerMask capa;
    public float Nafta;

    // Start is called before the first frame update
    void Start()
    {
        Nafta = NaftaMax;
    }


    void Update()
    {
        if (EstaVolando && Nafta > 0f)
        {
            Nafta -= Time.deltaTime;
            //rb.transform.Translate(transform.up * FuerzaJetpack * Time.deltaTime);
            rb.velocity = new Vector2(rb.velocity.x, FuerzaJetpack);
            //rb.AddForce(rb.transform.up * FuerzaJetpack * Time.deltaTime, ForceMode2D.Impulse);
            fire.Play();
        }
        else if (Physics2D.Raycast(pruebaPiso.position, Vector2.down, 0.05f, capa) && Nafta < NaftaMax)
        {
            Nafta += Time.deltaTime;
            fire.Stop();
        }
        else
        {
            fire.Stop();
        }
    }

    public void Funcionar(InputAction.CallbackContext callbackContext)
    {
            if (callbackContext.performed)
            {
                EstaVolando = true;
            }
            if (callbackContext.canceled)
            {
                EstaVolando = false;
            }
        //if (NumeroJugador == 2)
        //{
        //    if (Input.GetKey(KeyCode.UpArrow) && Nafta > 0f)
        //    {
        //        Nafta -= Time.deltaTime;
        //        rb.velocity = new Vector2(rb.velocity.x, FuerzaJetpack);
        //        fire.Play();
        //    }
        //    else if (Physics2D.Raycast(pruebaPiso.position, Vector2.down, 0.05f, capa) && Nafta < NaftaMax)
        //    {
        //        Nafta += 10 * Time.deltaTime;
        //        fire.Stop();
        //    }
        //    else
        //    {
        //        fire.Stop();
        //    }
        //}
    }

}

