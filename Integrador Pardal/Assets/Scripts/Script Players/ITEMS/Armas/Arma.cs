using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Arma : MonoBehaviour
{
    private float rotacion;
    private bool rotarActivado;
    [SerializeField] private float velocidadInclinacion;
    [SerializeField] private float LimiteRotacion;

    public GameObject Bala;
    [SerializeField] private float VelocidadBala;
    [SerializeField] private Transform zonaDisparo;

    [SerializeField] private float Cooldown;
    public float CooldownTime;
    public bool PuedeDisparar;
    private bool YaApreto;
    public Jugador Portador;
    [SerializeField] private GameObject Cargador;
    //[SerializeField] private GameObject cargador;

    public bool ArmaInclinable;
    public bool CaidaBala;


    void Start()
    {
        PuedeDisparar = false;

    }

    void Update()
    {
        ActivarCooldown();
        //if (ArmaInclinable) RotarArma();
        //if (PuedeDisparar)
        //{
        //    cargador.SetActive(true);
        //}
        //else
        //{
        //    cargador.SetActive(false); ;
        //}

    }

    //private void RotarArma()
    //{
    //    if (GetComponentInParent<Movimiento>().NumeroJugador == 1)
    //    {


    //        if (Input.GetKeyDown(KeyCode.F) && PuedeDisparar)
    //        {
    //            rotarActivado = true;
    //            YaApreto = true;
    //        }
    //        else if (Input.GetKeyUp(KeyCode.F) && PuedeDisparar && YaApreto)
    //        {
    //            GameObject bala = Instantiate(Bala, zonaDisparo.position, transform.rotation);
    //            bala.GetComponent<Rigidbody2D>().AddForce(zonaDisparo.right * VelocidadBala, ForceMode2D.Impulse);
    //            rotarActivado = false;
    //            transform.rotation = Quaternion.Euler(new Vector3(0, transform.eulerAngles.y, 0));
    //            rotacion = 0;
    //            PuedeDisparar = false;
    //            YaApreto = false;
    //        }
    //        if (rotarActivado)
    //        {
    //            rotacion += Time.deltaTime * velocidadInclinacion;
    //            if (rotacion <= LimiteRotacion)
    //                transform.rotation = Quaternion.Euler(new Vector3(0, transform.eulerAngles.y, rotacion));
    //        }

    //    }
    //    if (GetComponentInParent<Movimiento>().NumeroJugador == 2)
    //    {


    //        if (Input.GetKeyDown(KeyCode.RightControl) && PuedeDisparar)
    //        {
    //            rotarActivado = true;
    //            YaApreto = true;
    //        }
    //        else if (Input.GetKeyUp(KeyCode.RightControl) && PuedeDisparar && YaApreto)
    //        {
    //            GameObject bala = Instantiate(Bala, zonaDisparo.position, transform.rotation);
    //            bala.GetComponent<Rigidbody2D>().AddForce(zonaDisparo.right * VelocidadBala, ForceMode2D.Impulse);
    //            rotarActivado = false;
    //            transform.rotation = Quaternion.Euler(new Vector3(0, transform.eulerAngles.y, 0));
    //            rotacion = 0;
    //            PuedeDisparar = false;
    //            YaApreto = false;
    //        }
    //    }

    //    if (rotarActivado)
    //    {
    //        rotacion += Time.deltaTime * velocidadInclinacion;
    //        if (rotacion <= LimiteRotacion)
    //            transform.rotation = Quaternion.Euler(new Vector3(0, transform.eulerAngles.y, rotacion));
    //    }

    //}
    public void DispararNormal(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed && PuedeDisparar && GameManager.PuedeDisparar)
        {
                GameObject bala = Instantiate(Bala, zonaDisparo.position, transform.rotation);
                if (!CaidaBala) bala.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
                bala.GetComponent<Rigidbody2D>().AddForce(zonaDisparo.right * VelocidadBala, ForceMode2D.Impulse);
                PuedeDisparar = false;

        }
        //if (GetComponentInParent<Movimiento>().NumeroJugador == 2)
        //{
             
        //    if (Input.GetKeyDown(KeyCode.RightControl) && PuedeDisparar)
        //    {
        //        GameObject bala = Instantiate(Bala, zonaDisparo.position, transform.rotation);
        //        if (!CaidaBala) bala.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePositionY;
        //        bala.GetComponent<Rigidbody2D>().AddForce(zonaDisparo.right * VelocidadBala, ForceMode2D.Impulse);
        //        PuedeDisparar = false;
        //    }
        //}

    }
    private void ActivarCooldown()
    {
        if (!PuedeDisparar)
        {
            Cargador.SetActive(false);
            if (CooldownTime < Cooldown)
            {
                CooldownTime += Time.deltaTime;
            }
            else
            {
                CooldownTime = 0;
                PuedeDisparar = true;
                Cargador.SetActive(true);
            }
        }
    }

}
