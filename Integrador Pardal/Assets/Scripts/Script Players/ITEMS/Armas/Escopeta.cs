using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Escopeta : MonoBehaviour
{


    [SerializeField] private GameObject Bala;
    [SerializeField] private float VelocidadBala;
    [SerializeField] private Transform zonaDisparo;

    [SerializeField] private float Cooldown;
    private float CooldownTime;
    private bool PuedeDisparar;


    [SerializeField] private int municionInicial;
    [SerializeField] private int municionActual;

    [SerializeField] private Jugador Portador;


    private void Start()
    {
        Portador = GetComponentInParent<Jugador>();
        ActivarCooldown();
        municionActual = municionInicial;
    }

    private void Update()
    {
        ActivarCooldown();
        if (municionActual <= 0)
        {
            municionActual = municionInicial;
            Portador.VolverArmaNormal();
        }
    }

    public void DispararEscopeta(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed && PuedeDisparar)
        {
            GameObject bala = Instantiate(Bala, zonaDisparo.position, transform.rotation);
            bala.GetComponent<Rigidbody2D>().AddForce(zonaDisparo.right * VelocidadBala, ForceMode2D.Impulse);
            municionActual--;
            PuedeDisparar = false;
        }

        //if (GetComponentInParent<Movimiento>().NumeroJugador == 2)
        //{
        //    if (Input.GetKeyDown(KeyCode.RightControl) && PuedeDisparar)
        //    {
        //        GameObject bala = Instantiate(Bala, zonaDisparo.position, transform.rotation);
        //        bala.GetComponent<Rigidbody2D>().AddForce(zonaDisparo.right * VelocidadBala, ForceMode2D.Impulse);
        //        municionActual--;
        //        PuedeDisparar = false;
        //    }
        //}

    }
    private void ActivarCooldown()
    {
        if (!PuedeDisparar)
        {
            if (CooldownTime < Cooldown)
            {
                CooldownTime += Time.deltaTime;
            }
            else
            {
                CooldownTime = 0;
                PuedeDisparar = true;
            }
        }
    }


}
