using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cinemachine;
public class Jugador : MonoBehaviour
{
    CinemachineTargetGroup c;

    [Header("PLAYER PROPS")]
    public string Name;
    public int Points;
    public int NumeroDeJugador;
    [SerializeField] private Vector2 zonaDeath;
    public Color playerColor;

    [Header("PLAYER UI")]
    public TextMeshProUGUI NameUI;
    public TextMeshProUGUI PointsUI;
    public GameObject sumadorDePuntosUI;

    [Header("PLAYER GUN")]
    public List<GameObject> Armas;
    public GameObject ArmaNormal;

    [Header("PLAYER INVULNERABILIDAD")]
    [SerializeField] private float tiempoInvulnerabilidad;
    [SerializeField] private List<int> LayerNumber;
    [Range(0, 1)] public float alpha;
    [SerializeField] private List<SpriteRenderer> sprites;
    [SerializeField] private int PlayerLayerNumber;
    public bool invulnerable = false;
    private float tiempoInvulnerabilidadCount;

    [HideInInspector]
    public bool SumarActivado;
    private float tiempo;

    void Start()
    {
        //rb = GetComponent<Rigidbody2D>();
        GameManager.Spawn(gameObject, 0.1f);
        transform.position = new Vector3(transform.position.x, transform.position.y, 0);
    }

    private void Update()
    {

        sumadorDePuntosUI.transform.rotation = Quaternion.identity;
        if (SumarActivado)
        {
            sumadorDePuntosUI.SetActive(true);
            tiempo += Time.deltaTime;
            if (tiempo >= 1f)
            {
                Points++;
                tiempo = 0f;
            }
        }
        else sumadorDePuntosUI.SetActive(false);

        Invulnerable();
        //Invisibilidad();
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("block"))
        {
            transform.position = zonaDeath;
        }
    }
    public void VolverArmaNormal()
    {
        foreach (GameObject gun in Armas)
        {
            gun.SetActive(false);
        }
        ArmaNormal.SetActive(true);
    }
    //public IEnumerator Invulnerabilidad(float time)
    //{
    //    invulnerable = true;
    //    yield return new WaitForSeconds(time);
    //    invulnerable = false;
    //}
    private void Invulnerable()
    {
        if (invulnerable)
        {
            tiempoInvulnerabilidadCount += Time.deltaTime;
            if (tiempoInvulnerabilidadCount >= tiempoInvulnerabilidad)
            {
                invulnerable = false;
                tiempoInvulnerabilidadCount = 0f;
            }
        }
        if (invulnerable)
        {
            foreach (SpriteRenderer sprite in sprites)
            {
                Color auxColor = sprite.color;
                auxColor.a = alpha;
                sprite.color = auxColor;
            }

            foreach (int layer in LayerNumber)
            {
                Physics2D.IgnoreLayerCollision(PlayerLayerNumber, layer, true);
            }
        }
        else
        {

            foreach (int layer in LayerNumber)
            {
                Physics2D.IgnoreLayerCollision(PlayerLayerNumber, layer, false);
            }

            foreach (SpriteRenderer sprite in sprites)
            {
                Color auxColor = sprite.color;
                auxColor.a = 1;
                sprite.color = auxColor;
            }
        }
    }
    public void HacerInvulnerable()
    {
        invulnerable = true;
        tiempoInvulnerabilidadCount = 0;
    }

    public void Invisibilidad()
    {
        if (GetComponent<Movimiento>().direccion != 0)
        {
            foreach (SpriteRenderer sprite in sprites)
            {
                sprite.enabled = false;
            }
        }
        else
        {
            foreach (SpriteRenderer sprite in sprites)
            {
                sprite.enabled = true;
            }
        }
    }
}


