using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    private Rigidbody2D rb;

    [Header("Movimiento")]
    public float direccion = 0f;
    public float speed = 5f;
    [Range(0, 0.3f)] public float suavizado;
    private Vector3 velocidad = Vector3.zero;
    private bool LookDerecha;

    [Header("Salto")]
    public float fuerzaSalto = 5f;
    private bool estaEnPiso;
    [SerializeField] private LayerMask capa;

    [Header("Coyote Time")]
    //coyote time
    [SerializeField] private float coyoteTime;
    private float coyoteTimeCounter;
    private bool coyote;

    [Header("Input buffer")]
    //Input Buffer
    [SerializeField] private float jumpBufferTime;
    private float jumpBufferCounter;



    [Header("Knockback")]
    public Vector2 FuerzaEmpuje;
    [HideInInspector]
    public bool MovimientoActivado;

    [Header("Otros")]
    [SerializeField] public int NumeroJugador;
    [SerializeField] private SpriteRenderer jetpack;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        estaEnPiso = false;
        MovimientoActivado = true;
    }

    void Update()
    {
        if (NumeroJugador == 1) direccion = Input.GetAxisRaw("Horizontal") * speed;
        if (NumeroJugador == 2) direccion = Input.GetAxisRaw("HorizontalPlayer2") * speed;
        if (MovimientoActivado)
            Salto();
        if (LookDerecha)
        {
            jetpack.sortingOrder = 19;
        }
        else
        {
            jetpack.sortingOrder = 20;
        }
    }
    private void FixedUpdate()
    {
        if (MovimientoActivado)
            MoverPersonaje();
    }
    private void MoverPersonaje()
    {
        Vector3 velocidadObjetivo = new Vector2(direccion * Time.deltaTime, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocidadObjetivo, ref velocidad, suavizado);
        //Sprite flip

        if (direccion < 0 & !LookDerecha)
        {
            Girar();
        }
        else if (direccion > 0 && LookDerecha)
        {
            Girar();
        }
    }

    private void Salto()
    {



        Debug.DrawLine(transform.position, transform.position + Vector3.down * 1.1f, Color.green);
        Debug.DrawLine(transform.position + Vector3.left * 0.7f, transform.position + Vector3.left * 0.7f + Vector3.down * 1.1f, Color.yellow);
        Debug.DrawLine(transform.position + Vector3.right * 0.7f, transform.position + Vector3.right * 0.7f + Vector3.down * 1.1f, Color.yellow);



        if (Physics2D.Raycast(transform.position, Vector2.down, 1.1f, capa))
        {
            estaEnPiso = true;
            coyoteTimeCounter = coyoteTime;
            //ContadorDeSaltos = 2;
        }
        else
        {
            estaEnPiso = false;
            coyoteTimeCounter -= Time.deltaTime;
        }
        if (NumeroJugador == 1)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                jumpBufferCounter = jumpBufferTime;
            }
            else
            {
                jumpBufferCounter -= Time.deltaTime;
            }

            if (jumpBufferCounter > 0 && coyoteTimeCounter > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
                jumpBufferCounter = 0;
            }
            if (Input.GetKeyUp(KeyCode.W) && rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
                coyoteTimeCounter = 0;
            }
            //if (Input.GetKeyDown(KeyCode.W))
            //{
            //    inputBuffer.Enqueue(KeyCode.W);
            //    Invoke("eliminarInputBuffer", inputBufferTime);
            //}
            //if (estaEnPiso)
            //{

            //    if (inputBuffer.Count > 0)
            //    {
            //        if (inputBuffer.Peek() == KeyCode.W)
            //        {
            //            rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
            //            inputBuffer.Dequeue();
            //        }
            //    }
            //}
        }
        if (NumeroJugador == 2)
        {
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                jumpBufferCounter = jumpBufferTime;
            }
            else
            {
                jumpBufferCounter -= Time.deltaTime;
            }

            if (jumpBufferCounter > 0 && coyoteTimeCounter > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
                jumpBufferCounter = 0;
            }
            if (Input.GetKeyUp(KeyCode.UpArrow) && rb.velocity.y > 0)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
                coyoteTimeCounter = 0;
            }
        }
    }

    public IEnumerator DeshabilitarMovimiento(float time)
    {
        MovimientoActivado = false;
        yield return new WaitForSeconds(time);
        MovimientoActivado = true;
    }

    public void Empujarse(Vector2 point)
    {
        //rb.velocity = new Vector2(FuerzaEmpuje.x * point.x, FuerzaEmpuje.y);
        rb.velocity = new Vector2(FuerzaEmpuje.x * point.x, FuerzaEmpuje.y);
    }

    private void Girar()
    {
        LookDerecha = !LookDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            StartCoroutine(DeshabilitarMovimiento(1));
            Empujarse(collision.GetContact(0).normal);
        }
    }

    private void OnEnable()
    {
        MovimientoActivado = true;
    }

    private void MovimientoInvisible()
    {
    }
}
