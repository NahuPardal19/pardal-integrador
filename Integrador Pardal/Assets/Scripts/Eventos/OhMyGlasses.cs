using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OhMyGlasses : MonoBehaviour, IEvento
{
    private float time;
    [SerializeField] private float OhMyGlassesTime;
    private bool EventoActivado;
    [SerializeField] GameObject PostProcessing;

    public void ActivarEvento()
    {
        EventoActivado = false;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("OH MY GLASSES", Color.magenta);
    }

    void Start()
    {
        EventoActivado = true;
    }

    void Update()
    {
        if (!EventoActivado)
        {
            PostProcessing.SetActive(true);
            time = 0;
            EventoActivado = true;
        }
        if (EventoActivado)
        {
            if (time < OhMyGlassesTime)
            {
                time += Time.deltaTime;
            }
            else
            {
                PostProcessing.SetActive(false);
            }
        }
    }
}
