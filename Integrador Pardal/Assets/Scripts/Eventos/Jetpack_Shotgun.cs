using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jetpack_Shotgun : MonoBehaviour, IEvento
{
    [SerializeField] List<GameObject> Shotguns;
    [SerializeField] List<GameObject> Jetpacks;
    [SerializeField] List<GameObject> ArmasEmpujar;

    private bool controlador;
    private bool eventoActivado;
    private float time;
    [SerializeField] private float EventTime;

    public void ActivarEvento()
    {
        eventoActivado = false;
    }
    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("JETPACKS & SHOTGUNS", Color.gray);
    }

    void Start()
    {
        eventoActivado = true;
    }

    void Update()
    {
        if (!eventoActivado)
        {
            time = 0;
            foreach (GameObject shotgun in Shotguns)
            {
                shotgun.SetActive(true);
            }
            foreach (GameObject jetpack in Jetpacks)
            {
                jetpack.SetActive(true);
            }
            foreach (GameObject jugador in GameManager.ListaDeJugadores)
            {
                jugador.GetComponent<InputPruebas>().fuerzaSalto = 10;
            }
            foreach (GameObject arma in ArmasEmpujar)
            {
                arma.SetActive(false);
            }
            eventoActivado = true;
            controlador = true;
        }
        else
        {
            if (controlador)
            {
                if (time < EventTime)
                {
                    time += Time.deltaTime;
                }
                else
                {

                    foreach (GameObject shotgun in Shotguns)
                    {
                        shotgun.SetActive(false);
                    }
                    foreach (GameObject jetpack in Jetpacks)
                    {
                        jetpack.SetActive(false);
                    }
                    foreach (GameObject jugador in GameManager.ListaDeJugadores)
                    {
                        jugador.GetComponent<InputPruebas>().fuerzaSalto = 40;
                    }
                    foreach (GameObject arma in ArmasEmpujar)
                    {
                        arma.SetActive(true);
                    }
                    controlador = false;
                }
            }
        }
    }
}
