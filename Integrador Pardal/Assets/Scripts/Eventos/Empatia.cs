using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Empatia : MonoBehaviour , IEvento
{
    private float time;
    [SerializeField] private float TimePerTeletransporte;

    [SerializeField] private float TiempoEvento;
    private float time2;
    private bool eventoActivado;
    public void ActivarEvento()
    {
        eventoActivado = true;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("EMPATY", Color.cyan);
    }

    void Start()
    {
        
    }

    void Update()
    {
        if (eventoActivado)
        {
            if (time < TimePerTeletransporte)
            {
                time += Time.deltaTime;
            }
            else
            {
                //IntercambiarTransforms();
                time = 0;
            }
            if (time2 < TiempoEvento)
            {
                time2 += Time.deltaTime;
            }
            else
            {
                time2 = 0;
                eventoActivado = false;
            }
        }
    }

    //private void IntercambiarTransforms()
    //{
    //    List<Vector2> posiciones = new List<Vector2>();
    //    foreach (GameObject jugador in GameManager.ListaDeJugadores)
    //    {
    //        posiciones.Add(jugador.transform.position);
    //    }
    //    for (int i = 0; i < GameManager.ListaDeJugadores.Count; i++)
    //    {
    //        GameManager.ListaDeJugadores[i].transform.position = 
    //    }
    //    Vector2 transform1 = jugador1.position;
    //    jugador1.position = jugador2.position;
    //    jugador2.position = transform1;
    //}
}
