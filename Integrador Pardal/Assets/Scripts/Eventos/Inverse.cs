using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class Inverse : MonoBehaviour, IEvento
{
    private float time;
    [SerializeField] private float InverseTime;
    [SerializeField] private CinemachineVirtualCamera camera;
    private bool EventoActivado;
    


    public void ActivarEvento()
    {
        EventoActivado = false;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("UPSIDE DOWN", Color.green);
    }

    void Start()
    {
        EventoActivado = true;
    }

    void Update()
    {
        if (!EventoActivado)
        {
            camera.m_Lens.Dutch = 180;
            time = 0;
            EventoActivado = true;
        }
        if (EventoActivado)
        {
            if (time < InverseTime)
            {
                time += Time.deltaTime;
            }
            else
            {
                camera.m_Lens.Dutch = 0;
            }
        }
    }
}
