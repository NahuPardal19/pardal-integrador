using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuchPeople : MonoBehaviour , IEvento
{
    public float time;
    [SerializeField] private float TiempoEvento;
    private bool eventoActivado;
    private bool ActivarContador;

    [SerializeField] private List<Transform> WayPoints;
    [SerializeField] private List<GameObject> bots;
    [SerializeField] private int cantidadBots;
    public List<GameObject> objectsSpawneados;

    public void ActivarEvento()
    {
        eventoActivado = false;
        ActivarContador = true;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("Much People", Color.magenta);
    }

    void Start()
    {
        objectsSpawneados = new List<GameObject>();
        eventoActivado = false;
    }

    void Update()
    {
        if (ActivarContador)
        {
            if (!eventoActivado)
            {
                for (int i = 0; i < cantidadBots; i++)
                {
                    foreach (Transform transform in WayPoints)
                    {
                        objectsSpawneados.Add(Instantiate(bots[Random.Range(0, bots.Count)], transform.position, Quaternion.identity, transform));
                    }

                }
                eventoActivado = true;
            }
            if (eventoActivado)
            {

                if (time < TiempoEvento)
                {
                    time += Time.deltaTime;
                }
                else
                {
                    foreach (GameObject bot in objectsSpawneados)
                    {
                        //objectsSpawneados.Remove(bot);
                        Destroy(bot);
                    }
                    //for (int i = objectsSpawneados.Count-1; i >= 0; i--)
                    //{
                    //    objectsSpawneados.RemoveAt(i);
                    //    Destroy(objectsSpawneados[i]);
                    //}
                    objectsSpawneados = new List<GameObject>();

                    time = 0;
                    eventoActivado = false;
                    ActivarContador = false;
                }
            }
        }

    }
}
