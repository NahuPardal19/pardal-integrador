using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class RandomHill : MonoBehaviour , IEvento
{
    [SerializeField] private List<Transform> waypoints;
    [SerializeField] private GameObject hill;
    private Vector2 posicionInicial;

    private float time;
    [SerializeField] private float TimePerTeletransporte;

    [SerializeField] private float TiempoEvento;
    public float time2;
    private bool eventoActivado;

    public void ActivarEvento()
    {
        eventoActivado = true;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("RANDOM HILL", Color.magenta);
    }

    void Start()
    {
        posicionInicial = hill.transform.position;
    }

    void Update()
    {
        if (eventoActivado)
        {
            if (time < TimePerTeletransporte)
            {
                time += Time.deltaTime;
            }
            else
            {
                int rnd = Random.Range(0, waypoints.Count);
                hill.transform.position = waypoints[rnd].position;
                time = 0;
            }
            if (time2 < TiempoEvento)
            {
                time2 += Time.deltaTime;
            }
            else
            {
                hill.transform.position = posicionInicial;
                time2 = 0;
                eventoActivado = false;
            }
        }
    }
}
