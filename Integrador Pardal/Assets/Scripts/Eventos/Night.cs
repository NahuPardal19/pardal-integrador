using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.Universal;

public class Night : MonoBehaviour, IEvento
{
    private Light2D GlobalLight;

    public static bool EventoEnFuncion;
    private bool NocheActivada;

    private float time;
    [SerializeField] private float NightTime;


    void Start()
    {
        EventoEnFuncion = false;
        GlobalLight = GameObject.Find("Global Light 2D").GetComponent<Light2D>();
        NocheActivada = true;
    }

    void Update()
    {
        NightMode();
    }

    public void NightMode()
    {
        
        if (!NocheActivada)
        {
            time = 0;
            GlobalLight.intensity = 0;
            foreach (GameObject jugador in GameManager.ListaDeJugadores)
            {
                if (jugador.GetComponentInChildren<Light2D>())
                {
                    Light2D luzAux = jugador.GetComponentInChildren<Light2D>();
                    luzAux.enabled = true;
                }
            }
            NocheActivada = true;
            EventoEnFuncion = true;
        }
        else
        {
            if (time < NightTime)
            {
                time += Time.deltaTime;
            }
            else
            {
                foreach (GameObject jugador in GameManager.ListaDeJugadores)
                {
                    Light2D luzAux = jugador.GetComponentInChildren<Light2D>();
                    luzAux.enabled = false;
                }
                GlobalLight.intensity = 1;
                EventoEnFuncion = false;
            }
        }
    }

    public void ActivarEvento()
    {
        NocheActivada = false;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("NIGHT MODE", Color.white);
    }
}
