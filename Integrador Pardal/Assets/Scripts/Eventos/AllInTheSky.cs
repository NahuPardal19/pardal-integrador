using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AllInTheSky : MonoBehaviour, IEvento
{
    [SerializeField] Transform TP;
    //[SerializeField] List<GameObject> Jugadores;
    public void ActivarEvento()
    {
        foreach (GameObject pj in GameManager.ListaDeJugadores)
        {
            pj.SetActive(true);
            pj.transform.position = TP.position;
            pj.transform.position = new Vector3(pj.transform.position.x, pj.transform.position.y, 0);
        }
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("ALL IN THE SKY", Color.blue);
    }

    void Start()
    {
    }

    void Update()
    {

    }
}
