using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class OneFloor : MonoBehaviour, IEvento
{
    [SerializeField] private CinemachineVirtualCamera camera;
    private SpawnEventoOneFloor inicioSpawner;


    private bool eventoActivado;
    private float time;
    [SerializeField] private float OnefloorTime;
    /* [HideInInspector]*/
    public static bool EmpezoEvento;


    public void ActivarEvento()
    {
        eventoActivado = false;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("ONE FLOOR", Color.yellow);
    }

    void Start()
    {
        eventoActivado = true;
        inicioSpawner = FindObjectOfType<SpawnEventoOneFloor>();
    }

    void Update()
    {
        if (!eventoActivado)
        {
            UI_MANAGER.LlamarEvento("One Floor", Color.red);
            time = 0;
            camera.Priority = 11;
            foreach (GameObject jugador in GameManager.ListaDeJugadores)
            {
                StartCoroutine(inicioSpawner.Spawnear(jugador));
            }

            eventoActivado = true;
            EmpezoEvento = true;
        }
        else
        {
            if (EmpezoEvento)
            {
                if (time < OnefloorTime)
                {
                    time += Time.deltaTime;
                }
                else
                {
                    camera.Priority = 9;
                    foreach (GameObject jugador in GameManager.ListaDeJugadores)
                    {
                        StartCoroutine(GameManager.Spawn(jugador, 1f));
                        jugador.transform.position = new Vector3(jugador.transform.position.x, jugador.transform.position.y, 0);
                        jugador.GetComponent<InputPruebas>().DeshabilitarMovimiento(1f);
                        EmpezoEvento = false;
                    }

                }
            }
        }
    }
}

