using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SuperPlayers : MonoBehaviour, IEvento
{
    private bool eventoActivado;
    private float time;
    [SerializeField] private float SuperTime;

    [SerializeField] private float AumentoSpeed;
    [SerializeField] private float AumentoJump;

    [SerializeField] private float InicioSpeed;
    [SerializeField] private float InicioJump;

    [SerializeField] List<TrailRenderer> trails;

    public void ActivarEvento()
    {
        eventoActivado = false;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("NICE RED BULL", Color.red);
    }

    void Start()
    {
        eventoActivado = true;

    }
    void Update()
    {
        if (!eventoActivado)
        {
            time = 0;
            foreach (GameObject jugador in GameManager.ListaDeJugadores)
            {
                jugador.GetComponent<InputPruebas>().speed = AumentoSpeed;
                jugador.GetComponent<InputPruebas>().fuerzaSalto = AumentoJump;

            }
            foreach (TrailRenderer trail in trails)
            {
                trail.enabled = true;
            }
            eventoActivado = true;
        }
        else
        {

            if (time < SuperTime)
            {
                time += Time.deltaTime;
            }
            else
           {
                foreach (GameObject jugador in GameManager.ListaDeJugadores)
                {
                    jugador.GetComponent<InputPruebas>().speed = InicioSpeed;
                    jugador.GetComponent<InputPruebas>().fuerzaSalto = InicioJump;
                }
                foreach (TrailRenderer trail in trails)
                {
                    trail.enabled = false;
                }

            }
        }
    }
}
