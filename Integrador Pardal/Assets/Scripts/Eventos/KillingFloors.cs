using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class KillingFloors : MonoBehaviour, IEvento
{
    private bool PisosEncontrados;
    private bool EventoActivado;

    public List<GameObject> Pisos;

    private float time;
    [SerializeField] private float TimePerBloque;
    [SerializeField] private int NumerosDePisosARomper;
    [SerializeField] private float TiempoEvento;
    [SerializeField] private float time2;

    public List<GameObject> PisosADestruir;

    void Start()
    {
        Pisos = new List<GameObject>();
        PisosEncontrados = false;
        Pisos = GameObject.FindGameObjectsWithTag("Floor").ToList();
    }
    void Update()
    {
        ActKillingFloors();
    }

    public void ActKillingFloors()
    {
        if (PisosEncontrados)
        {
            Pisos = new List<GameObject>();
            Pisos = GameObject.FindGameObjectsWithTag("Floor").ToList();
            PisosEncontrados = false;
        }

        if (EventoActivado)
        {

            if (time2 < TiempoEvento)
            {
                time2 += Time.deltaTime;
            }
            else
            {
                EventoActivado = false;
                time2 = 0;

            }
            if (time < TimePerBloque)
            {
                time += Time.deltaTime;
            }
            else
            {
                PisosADestruir = new List<GameObject>();
                for (int i = 0; i < NumerosDePisosARomper; i++)
                {
                    int rnd = Random.Range(0, Pisos.Count);
                    if(Pisos[rnd].activeInHierarchy)
                    PisosADestruir.Add(Pisos[rnd]);
                }
                StartCoroutine(DestruirDespuesDeEspera( PisosADestruir));
                time = 0;
            }
        }

    }

    IEnumerator DestruirDespuesDeEspera( List<GameObject> PisosADestruir)
    {
        foreach (GameObject piso in PisosADestruir)
        {
            piso.GetComponent<SpriteRenderer>().color = Color.red; 
        }

        yield return new WaitForSeconds(2);

        foreach (GameObject piso in PisosADestruir)
        {
            if (piso.activeInHierarchy)
            {
                piso.SetActive(false);
            }
        }

        yield return new WaitForSeconds(8);


        foreach (GameObject piso in PisosADestruir)
        {
            piso.SetActive(true);
            piso.GetComponent<SpriteRenderer>().color = Color.white;
        }

    }

    public void ActivarEvento()
    {
        PisosEncontrados = true;
        EventoActivado = true;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("KILLING FLOORS", Color.red);
    }
}
