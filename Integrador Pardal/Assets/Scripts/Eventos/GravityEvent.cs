using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityEvent : MonoBehaviour , IEvento
{
    private float time;
    [SerializeField] private float GravityTime;
    private bool EventoActivado;

    public void ActivarEvento()
    {
        EventoActivado = false;
    }

    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("GRAVITY", Color.green);
    }

    void Start()
    {
        EventoActivado = true;

    }

    void Update()
    {
        if (!EventoActivado)
        {
            foreach (GameObject jugador in GameManager.ListaDeJugadores)
            {
                jugador.GetComponent<Rigidbody2D>().gravityScale = 3;
            }
            time = 0;
            EventoActivado = true;
        }
        if (EventoActivado)
        {
            if (time < GravityTime)
            {
                time += Time.deltaTime;
            }
            else
            {
                foreach (GameObject jugador in GameManager.ListaDeJugadores)
                {
                    jugador.GetComponent<Rigidbody2D>().gravityScale = 10;
                }
            }
        }
    }
}
