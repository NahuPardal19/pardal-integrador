using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ECaidaDeBloques : MonoBehaviour, IEvento
{
    [SerializeField] float PuntoIzqX;
    [SerializeField] float PuntoDerX;
    [SerializeField] float PuntoY;

    [SerializeField] GameObject Bloque;

    private float time;
    [SerializeField] private float TimePerBloque;

    [SerializeField] private float TiempoEvento;
    private float time2;
    private bool eventoActivado;

    void Start()
    {

    }

    void Update()
    {
        if (eventoActivado)
        {
            if (time < TimePerBloque)
            {
                time += Time.deltaTime;
            }
            else
            {
                float n = Random.Range(PuntoIzqX, PuntoDerX);
                Instantiate(Bloque, new Vector2(n, PuntoY), Bloque.transform.rotation);
                time = 0;
            }
            if (time2 < TiempoEvento)
            {
                time2 += Time.deltaTime;
            }
            else
            {
                time2 = 0;
                eventoActivado = false;
            }
        }


    }
    public void ActivarEvento()
    {
        eventoActivado = true;
    }


    public void MostrarEvento()
    {
        UI_MANAGER.LlamarEvento("BLOCK RAIN", Color.red);
    }
}
