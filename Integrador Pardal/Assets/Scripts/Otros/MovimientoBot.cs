using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoBot : MonoBehaviour
{
    private Rigidbody2D rb;

    private float velocidad;
    [SerializeField] private Vector2 velocidadRandom;

    [SerializeField] private Transform floorController;

    private bool LookDerecha;
    private bool yaTocoSuelo;
    [SerializeField] private float RayCast = 3f;

    private float time;
    [SerializeField] private Vector2 RandomtimePerJump;
    private float SaltoRandomTime;
    [SerializeField] private int JumpForce;
    void Start()
    {
        velocidad = Random.Range(velocidadRandom.x, velocidadRandom.y);

        rb = GetComponent<Rigidbody2D>();
        LookDerecha = false;
        RandomTimePerJump();
        //Physics2D.IgnoreLayerCollision(PlayerLayerNumber, layer, true)

    }

    void Update()
    {
        

    }
    private void FixedUpdate()
    {
        if (yaTocoSuelo)
            MovimientoAleatorio();
        Jump();
    }

    public void MovimientoAleatorio()
    {
        RaycastHit2D tocoSuelo = Physics2D.Raycast(floorController.position, Vector2.down, RayCast);
        rb.velocity = new Vector2(velocidad, rb.velocity.y);
        if (tocoSuelo == false)
        {
            Girar();
        }
    }

    private void Girar()
    {
        LookDerecha = !LookDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
        velocidad *= -1;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(floorController.position, floorController.transform.position + Vector3.down * RayCast);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("ground"))
        {
            yaTocoSuelo = true;
        }
    }

    private void Jump()
    {
        if (time < SaltoRandomTime)
        {
            time += Time.deltaTime;
        }
        else
        {
            rb.velocity = new Vector2(rb.velocity.x, JumpForce);
            time = 0;
            RandomTimePerJump();
        }
    }

    private void RandomTimePerJump()
    {
        SaltoRandomTime = Random.Range(RandomtimePerJump.x, RandomtimePerJump.y);
    }


}
