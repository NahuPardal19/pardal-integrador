using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEventoOneFloor : MonoBehaviour
{
    [SerializeField] private Transform xIzquierda;
    [SerializeField] private Transform xDerecha;
    [SerializeField] private Transform Y;


    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player") && OneFloor.EmpezoEvento)
        {
            StartCoroutine(Spawnear(collision.gameObject));
        }
    }

    public IEnumerator Spawnear(GameObject jugador)
    {
        jugador.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        yield return new WaitForSeconds(1);
        jugador.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        if (OneFloor.EmpezoEvento)
            jugador.transform.position = new Vector2(Random.Range(xIzquierda.position.x, xDerecha.position.x), Y.position.y);
    }
}
