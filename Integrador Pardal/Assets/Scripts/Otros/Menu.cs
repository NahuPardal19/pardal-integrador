using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject menuPausa;
    void Start()
    {

    }


    void Update()
    {

    }



    public void Reanudar()
    {
        Time.timeScale = 1;
        menuPausa.SetActive(false);
        Cursor.visible = false;
    }

    public void Jugar(int numeroEscena)
    {
        SceneManager.LoadScene(numeroEscena);
        Time.timeScale = 1;
    }

    public void Salir()
    {
        print("saliste");
        Application.Quit();

    }
    public void Desactivar(GameObject menuBorrar)
    {
        menuBorrar.SetActive(false);
    }
    public void Activar(GameObject MenuCrear)
    {
        MenuCrear.SetActive(true);
    }
}
