using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class InputPruebas : MonoBehaviour
{
    private Rigidbody2D rb;
    PlayerInput playerInput;

    private bool act;

    public int PlayerID;

    [Header("Movimiento")]
    public float direccion;
    public float speed;
    [Range(0, 0.3f)] public float suavizado;
    private Vector3 velocidad = Vector3.zero;
    private bool LookDerecha;

    [Header("Salto")]
    private bool isJumping;
    public float fuerzaSalto = 5f;
    public bool estaEnPiso;
    [SerializeField] private LayerMask capa;

    [Header("Coyote Time")]
    //coyote time
    [SerializeField] private float coyoteTime;
    public float coyoteTimeCounter;

    [Header("Input buffer")]
    [SerializeField] private float jumpBufferTime;
    public float jumpBufferCounter;

    [Header("Knockback")]
    public Vector2 FuerzaEmpuje;
    [HideInInspector]
    public bool MovimientoActivado;


    [SerializeField] private SpriteRenderer jetpack;


    void Start()
    {

        MovimientoActivado = true;
        rb = GetComponent<Rigidbody2D>();
        playerInput = GetComponent<PlayerInput>();
        AsignarDevice();
    }

    void Update()
    {
        direccion = playerInput.actions["Move"].ReadValue<Vector2>().x * speed;

        Debug.DrawLine(transform.position, transform.position + Vector3.down * 1.1f, Color.green);

        if (Physics2D.Raycast(transform.position, Vector2.down, 1.1f, capa))
        {
            estaEnPiso = true;
            coyoteTimeCounter = coyoteTime;
            //ContadorDeSaltos = 2;
        }
        else
        {
            estaEnPiso = false;
            coyoteTimeCounter -= Time.deltaTime;
        }

        if (jumpBufferCounter > 0 && coyoteTimeCounter > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, fuerzaSalto);
            jumpBufferCounter = 0;


            if (!isJumping)
            {
                rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
            }
        }
        else
        {
            jumpBufferCounter -= Time.deltaTime;
        }

        if (jetpack != null)
        {
            if (LookDerecha)
            {
                jetpack.sortingOrder = 19;
            }
            else
            {
                jetpack.sortingOrder = 20;
            }
        }

        if (direccion != 0 && !act)
        {
            act = true;
            ActivarJugador();
        }

    }

    private void FixedUpdate()
    {
        if (MovimientoActivado)
            MoverPersonaje();
    }


    private void MoverPersonaje()
    {
        Vector3 velocidadObjetivo = new Vector2(direccion * Time.deltaTime, rb.velocity.y);
        rb.velocity = Vector3.SmoothDamp(rb.velocity, velocidadObjetivo, ref velocidad, suavizado);

        //Sprite flip

        if (direccion < 0 & !LookDerecha)
        {
            Girar();
        }
        else if (direccion > 0 && LookDerecha)
        {
            Girar();
        }
    }

    private void Girar()
    {
        LookDerecha = !LookDerecha;
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);

    }

    public void Jump(InputAction.CallbackContext callbackContext)
    {

        if (callbackContext.performed)
        {
            ActivarJugador();
            isJumping = true;
            jumpBufferCounter = jumpBufferTime;
        }

        if (callbackContext.canceled)
        {
            isJumping = false;

        }
        if (rb.velocity.y > 0f)
        {
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.5f);
            coyoteTimeCounter = 0f;

        }

    }


    public IEnumerator DeshabilitarMovimiento(float time)
    {
        MovimientoActivado = false;
        yield return new WaitForSeconds(time);
        MovimientoActivado = true;
    }

    public void Empujarse(Vector2 point)
    {
        //rb.velocity = new Vector2(FuerzaEmpuje.x * point.x, FuerzaEmpuje.y);
        rb.velocity = new Vector2(FuerzaEmpuje.x * point.x, FuerzaEmpuje.y);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Bala"))
        {
            StartCoroutine(DeshabilitarMovimiento(1));
            Empujarse(collision.GetContact(0).normal);
        }
    }

    private void OnEnable()
    {
        MovimientoActivado = true;
    }

    private void ActivarJugador()
    {
        rb.constraints = RigidbodyConstraints2D.None;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    public void AsignarDevice()
    {
        if (PlayerID == 1)
            playerInput.SwitchCurrentControlScheme("Keyboard ASWD", Keyboard.current);

        if (PlayerID == 2)
            playerInput.SwitchCurrentControlScheme("Keyboard arrows", Keyboard.current);

        if (PlayerID == 3)
        {
            if (Joystick.all.Count == 1 && Gamepad.all.Count == 1)
            {
                playerInput.SwitchCurrentControlScheme("Joystick", Joystick.all[0]);
            }
            else if (Gamepad.all.Count >= 1)
                playerInput.SwitchCurrentControlScheme("Gamepad", Gamepad.all[0]);
            else if (Joystick.all.Count >= 1)
            {
                playerInput.SwitchCurrentControlScheme("Joystick", Joystick.all[0]);
            }
        }
        if (PlayerID == 4)
        {
            if (Joystick.all.Count == 1 && Gamepad.all.Count == 1)
            {
                playerInput.SwitchCurrentControlScheme("Gamepad", Gamepad.all[0]);
            }
            else if (Gamepad.all.Count > 1)
            {
                playerInput.SwitchCurrentControlScheme("Gamepad", Gamepad.all[1]);
            }
            else if (Joystick.all.Count > 1)
            {
                playerInput.SwitchCurrentControlScheme("Joystick", Joystick.all[1]);
            }


        }

    }

    public void SeDesconecto()
    {
        print("desconexion");
    }
    public void SeReconecto()
    {
        print("reconexion");
    }
    public void Change()
    {
        print("change");
    }
}


