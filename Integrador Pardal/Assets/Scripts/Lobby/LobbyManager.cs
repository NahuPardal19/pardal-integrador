using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;

public class LobbyManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> AllJugadores;
    public List<int> ListaDeJugadores;
    public static LobbyManager instance;
    [SerializeField] private GameObject iniciador;
    [SerializeField] private GameObject textoJoin;
    [SerializeField] private GameObject textoCantidadJugadores;

    private List<InputDevice> connectedDevices = new List<InputDevice>();

    private bool contadorBool;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);


    }
    void Start()
    {
        AllJugadores = GameObject.FindGameObjectsWithTag("Player").ToList();
        iniciador = GameObject.Find("IniciadorPartida");
        textoJoin = GameObject.Find("TextJoinText");
        textoCantidadJugadores = GameObject.Find("CantidadPlayersText");

        GameManager.PuedeDisparar = true;
        Cursor.visible = false;
        List<InputDevice> devices = InputSystem.devices.ToList();
        connectedDevices.AddRange(devices);
    }

    void Update()
    {
        if (ListaDeJugadores.Count > 1 && iniciador != null)
        {
            iniciador.GetComponent<Collider2D>().enabled = true;
            iniciador.GetComponent<SpriteRenderer>().enabled = true;
            textoCantidadJugadores.GetComponent<TextMeshProUGUI>().color = Color.white;

        }
        else
        {
            if (iniciador != null)
            {
                iniciador.GetComponent<Collider2D>().enabled = false;
                iniciador.GetComponent<SpriteRenderer>().enabled = false;
                textoCantidadJugadores.GetComponent<TextMeshProUGUI>().color = Color.red;
            }
        }
        if (textoCantidadJugadores != null)
        {
            if (ListaDeJugadores.Count > 0)
            {
                textoJoin.SetActive(false);
            }
            textoCantidadJugadores.GetComponent<TextMeshProUGUI>().text = ListaDeJugadores.Count.ToString() + "/4";
        }

        Verificacion();
        BorrarLista();
    }

    public void AgregarNuevoJugador(int id)
    {
        ListaDeJugadores.Add(id);
    }

    private void Verificacion()
    {
        InputDevice[] devices = InputSystem.devices.ToArray();

        foreach (InputDevice device in devices)
        {
            if (!connectedDevices.Contains(device))
            {
                // Nuevo dispositivo conectado
                Debug.Log("Se conectů un dispositivo: " + device.displayName);
                if (SceneManager.GetActiveScene().name == "Selector de Personajes")
                {
                    foreach (GameObject jugador in AllJugadores)
                    {
                        jugador.GetComponent<InputPruebas>().AsignarDevice();
                    }
                    connectedDevices.Add(device);
                }
            }
        }

        for (int i = connectedDevices.Count - 1; i >= 0; i--)
        {

            if (!devices.Contains(connectedDevices[i]))
            {
                // Dispositivo desconectado
                Debug.Log("Se desconectů un dispositivo: " + connectedDevices[i].displayName);
                connectedDevices.RemoveAt(i);
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            bool puedeAgregar = true;
            foreach (int id in ListaDeJugadores)
            {
                if (collision.GetComponent<InputPruebas>().PlayerID == id)
                {
                    puedeAgregar = false;
                }
            }
            if (puedeAgregar)
                ListaDeJugadores.Add(collision.GetComponent<InputPruebas>().PlayerID);
        }
    }

    private void BorrarLista()
    {

        if (SceneManager.GetActiveScene().name != "Selector de Personajes")
        {
            
            AllJugadores = new List<GameObject>();
            //AkSoundEngine.PostEvent("StopMusicaLobby", gameObject);
        }
        else
        {
            AllJugadores = GameObject.FindGameObjectsWithTag("Player").ToList();

            //AkSoundEngine.PostEvent("StopMusicaNormal", gameObject);

            if (iniciador == null)
            iniciador = GameObject.Find("IniciadorPartida");

            if (textoJoin == null)
                textoJoin = GameObject.Find("TextJoinText");

            if (textoCantidadJugadores == null)
                textoCantidadJugadores = GameObject.Find("CantidadPlayersText");
        }

        if (SceneManager.GetActiveScene().name == "Menu")
        {
            //AkSoundEngine.PostEvent("StopMusicaNormal", gameObject);
            //AkSoundEngine.PostEvent("StopMusicaLobby", gameObject);

        }
    }
}
