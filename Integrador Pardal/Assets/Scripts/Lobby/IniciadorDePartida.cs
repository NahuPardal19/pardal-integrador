using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class IniciadorDePartida : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI contadorText;
    private float contador;
    private int contadorRedondeado;
    [SerializeField] private List<GameObject> ListaJugadoresArriba;
    LobbyManager lobbyManager;
    private bool reloj;

    void Start()
    {
        lobbyManager = FindObjectOfType<LobbyManager>();
    }

    void Update()
    {
        ActivarContador();
        IniciarPartida();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ListaJugadoresArriba.Add(collision.gameObject);
        }
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            ListaJugadoresArriba.Remove(collision.gameObject);
        }
    }
    private void IniciarPartida()
    {
        if (contador < 0)
        {
            SceneManager.LoadScene(2);
        }
    }
    public void ActivarContador()
    {
        if (ListaJugadoresArriba.Count > 0)
        {
            contadorText.enabled = true;

            contador -= Time.deltaTime;
            contadorRedondeado = Mathf.RoundToInt(contador);
            contadorText.text = contadorRedondeado.ToString();
            GetComponent<SpriteRenderer>().color = Color.green;
           


        }
        else
        {
            contadorText.enabled = false;
            contador = 5f; 
            GetComponent<SpriteRenderer>().color = Color.white;

        }
    }

}
