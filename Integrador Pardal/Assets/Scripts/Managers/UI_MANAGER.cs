using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class UI_MANAGER : MonoBehaviour
{
    [SerializeField] private static Animator animacionEvento;
    private static TextMeshProUGUI textoEvento;
    [SerializeField] private TextMeshProUGUI textoTime;


    private void Start()
    {
        textoEvento = GameObject.Find("text evento").GetComponent<TextMeshProUGUI>();
        animacionEvento = FindObjectOfType<Animator>();
    }
    private void Update()
    {
        Score_UI_Manager();
        MostrarTiempo();

        if (GameManager.MuerteSubita)
        {
            textoTime.text = "DEAD";
        }
    }

    public void Score_UI_Manager()
    {
        foreach (GameObject player in GameManager.ListaDeJugadores)
        {
            player.GetComponent<Jugador>().NameUI.text = player.GetComponent<Jugador>().Name;
            player.GetComponent<Jugador>().NameUI.color = player.GetComponent<Jugador>().playerColor;
            player.GetComponent<Jugador>().PointsUI.text = player.GetComponent<Jugador>().Points.ToString();
        }
    }

    public static void LlamarEvento(string nombreEvento, Color colorTexto)
    {
        textoEvento.text = nombreEvento;
        textoEvento.color = colorTexto;
        animacionEvento.SetTrigger("EVENTO");
    }

    public void MostrarTiempo()
    {
        int minutos = (int)GameManager.time; 
        int segundos = Mathf.RoundToInt((GameManager.time - minutos) * 60f); 

        string tiempoFormateado = string.Format("{0}:{1:D2}", minutos, segundos);
        textoTime.text = tiempoFormateado;
    }


}
