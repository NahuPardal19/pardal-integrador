using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EventosManager : MonoBehaviour
{
    [SerializeField] private List<IEvento> ListaDeEventos;
    [SerializeField] private float TimePerEvento;

    [SerializeField] private float time;
    void Start()
    {
        ListaDeEventos = new List<IEvento>();
        ListaDeEventos = FindObjectsOfType<MonoBehaviour>().OfType<IEvento>().ToList();

    }

    void Update()
    {
        if (time < TimePerEvento)
        {
            time += Time.deltaTime;
        }
        else
        {
            int rnd = Random.Range(0, ListaDeEventos.Count);
            ListaDeEventos[rnd].ActivarEvento();
            ListaDeEventos[rnd].MostrarEvento();
            time = 0;
        }
    }
}
