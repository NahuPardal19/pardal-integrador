using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Linq;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.Rendering.Universal;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    public static bool MuerteSubita;

    [SerializeField] public static List<GameObject> ListaDeJugadores;
    [SerializeField] private List<GameObject> ListaDeTodosJugadores;
    private static List<GameObject> PlayersAlive;

    private static CinemachineTargetGroup cinemachineTargetGroup;

    [Header("PLAYERS SPAWN")]
    [SerializeField] private float TimeToSpawn;

    [SerializeField] Transform puntoXizq;
    [SerializeField] Transform puntoXder;
    private static float staticPuntoXizq;
    private static float staticPuntoXder;
    private static float staticPuntoY;
    private static float StaticTimeToSpawn;

    public static bool PuedeDisparar;

    [Header("PLAYERS SPAWN")]
    [SerializeField] private GameObject menuPausa;

    [Header("WIN CONDITION")]
    [SerializeField] private GameObject menuWin;
    [SerializeField] private TextMeshProUGUI textwin;
    [SerializeField] private int PointsToWin;
    [HideInInspector] public static float time;
    [SerializeField] private float MaxGameTime;
    [SerializeField] private GameObject TiempoUI;

    [SerializeField] private GameObject botonSelectGameOver;
    [SerializeField] private GameObject botonSelectPause;
    private EventSystem eventSystem;
    bool botonGameOverSeleccionado;

    private LobbyManager lobbyManager;

    private bool EndGame;

    void Start()
    {
        ListaDeJugadores = new List<GameObject>();
        PlayersAlive = new List<GameObject>();
        time = MaxGameTime;
        Time.timeScale = 1;
        StaticTimeToSpawn = TimeToSpawn;
        staticPuntoXizq = puntoXizq.position.x;
        staticPuntoXder = puntoXder.position.x;
        staticPuntoY = puntoXizq.position.y;
        MuerteSubita = false;
        eventSystem = FindObjectOfType<EventSystem>();

        IniciarPartida();

        cinemachineTargetGroup = FindObjectOfType<CinemachineTargetGroup>();
        foreach (GameObject p in ListaDeJugadores)
        {
            cinemachineTargetGroup.AddMember(p.transform, 10, 1);
        }

        Cursor.visible = false;


        PuedeDisparar = true;
    }

    void Update()
    {
        CalcularTiempoPartida();
        WinCondition();
    }

    public void CalcularTiempoPartida()
    {
        if (!MuerteSubita)
        {
            if (time < 0f)
            {
                EndGame = true;
            }
            else
            {
                time -= Time.deltaTime;
            }
        }
    }
    public void WinCondition()
    {
        if (!EndGame)
        {
            //si es a puntos y no por tiempo scriptear aca
        }
        else
        {
            int pointMaxAux = 0;

            List<GameObject> ListaDeJugadoresNoEmpatados = new List<GameObject>();
            List<GameObject> ListaDeJugadoresEmpatados = new List<GameObject>();

            foreach (GameObject jugador in ListaDeJugadores)
            {
                if (jugador.GetComponent<Jugador>().Points > pointMaxAux)
                {
                    pointMaxAux = jugador.GetComponent<Jugador>().Points;
                }
            }

            foreach (GameObject jugador in ListaDeJugadores)
            {
                if (jugador.GetComponent<Jugador>().Points == pointMaxAux)
                {
                    ListaDeJugadoresEmpatados.Add(jugador);
                }
                else if (jugador.GetComponent<Jugador>().Points < pointMaxAux)
                {
                    ListaDeJugadoresNoEmpatados.Add(jugador);
                }
            }

            if (ListaDeJugadoresEmpatados.Count == 1)
            {
                textwin.text = "Gano: " + ListaDeJugadoresEmpatados[0].GetComponent<Jugador>().Name;
                if (!botonGameOverSeleccionado)
                {
                    eventSystem.SetSelectedGameObject(botonSelectGameOver);
                    botonGameOverSeleccionado = true;
                }
            }
            else
            {
                MuerteSubita = true;
                MuerteSubitaEvent(ListaDeJugadoresNoEmpatados);
            }


            if (!MuerteSubita)
            {
                Time.timeScale = 0;
                menuWin.SetActive(true);
                TiempoUI.SetActive(false);
            }
            if (MuerteSubita)
            {
                if (PlayersAlive.Count == 1)
                {
                    textwin.text = "Gano: " + PlayersAlive[0].GetComponent<Jugador>().Name;
                    menuWin.SetActive(true);
                    TiempoUI.SetActive(false);
                    if(!botonGameOverSeleccionado)
                    {
                        eventSystem.SetSelectedGameObject(botonSelectGameOver);
                        botonGameOverSeleccionado = true;

                    }
                }
            }
        }
    }
    public void MuerteSubitaEvent(List<GameObject> JugadoresAEliminar)
    {
        if (MuerteSubita)
        {
            UI_MANAGER.LlamarEvento("Sudden Death", Color.red);
            GetComponent<EventosManager>().enabled = false;
            foreach (GameObject jugador in JugadoresAEliminar)
            {
                cinemachineTargetGroup.RemoveMember(jugador.transform);
                PlayersAlive.Remove(jugador);
                jugador.SetActive(false);
            }

        }
    }
    public static IEnumerator Spawn(GameObject jugador)
    {
        jugador.transform.position = new Vector2(Random.Range(staticPuntoXizq, staticPuntoXder), staticPuntoY);
        jugador.transform.position = new Vector3(jugador.transform.position.x, jugador.transform.position.y, 0);
        FuncionJugador(false, jugador);
        PuedeDisparar = false;
        yield return new WaitForSeconds(StaticTimeToSpawn);
        FuncionJugador(true, jugador);
        jugador.GetComponent<Jugador>().HacerInvulnerable();
        PuedeDisparar = true;
    }
    public static IEnumerator Spawn(GameObject jugador, float time)
    {
        jugador.transform.position = new Vector2(Random.Range(staticPuntoXizq, staticPuntoXder), staticPuntoY);
        jugador.transform.position = new Vector3(jugador.transform.position.x, jugador.transform.position.y, 0);

        yield return new WaitForSeconds(time);
        FuncionJugador(true, jugador);
        //jugador.GetComponent<Jugador>().HacerInvulnerable();
    }
    public void Pausar(InputAction.CallbackContext callbackContext)
    {
        if (callbackContext.performed)
        {
            if (!menuPausa.activeInHierarchy)
            {
                eventSystem.SetSelectedGameObject(botonSelectPause);
                Time.timeScale = 0;
                menuPausa.SetActive(true);
                Cursor.visible = true;

            }
            else
            {
                Time.timeScale = 1;
                menuPausa.SetActive(false);
                Cursor.visible = false;
            }
        }
    }
    private void IniciarPartida()
    {
        lobbyManager = FindObjectOfType<LobbyManager>();
        foreach (GameObject jugador in ListaDeTodosJugadores)
        {
            foreach (int id in lobbyManager.ListaDeJugadores)
            {
                if (jugador.GetComponent<InputPruebas>().PlayerID == id)
                {
                    jugador.SetActive(true);
                    ListaDeJugadores.Add(jugador);
                    PlayersAlive.Add(jugador);
                }
            }
        }
    }
    private static void FuncionJugador(bool b, GameObject g)
    {
        g.GetComponent<Collider2D>().enabled = b;
        g.GetComponent<Jugador>().enabled = b;
        g.GetComponent<InputPruebas>().enabled = b;

        if (b)
        {
            g.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            g.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        }

        else g.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezePosition;

        g.GetComponent<SpriteRenderer>().enabled = b;
        //List<SpriteRenderer> allSprites = g.GetComponentsInChildren<SpriteRenderer>().ToList();
        //foreach (SpriteRenderer sprite in allSprites)
        //{
        //    sprite.enabled = b;
        //}

        foreach (Transform child in g.transform)
        {
            if (child.gameObject.name != "Luz")
                child.gameObject.SetActive(b);
            else
            {
                if (Night.EventoEnFuncion)
                    child.GetComponent<Light2D>().enabled = b;
            }
        }

    }
    public static void EliminarJugador(GameObject jugador)
    {
        jugador.SetActive(false);
        cinemachineTargetGroup.RemoveMember(jugador.transform);
        PlayersAlive.Remove(jugador);
    }
}
