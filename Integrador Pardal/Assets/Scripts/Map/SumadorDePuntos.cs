using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SumadorDePuntos : MonoBehaviour
{
    public Jugador JugadorActual;

    public List<Jugador> jugadoresDentro;

    [SerializeField] private SpriteRenderer hill;

    private bool SonidoActivado = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            jugadoresDentro.Add(collision.gameObject.GetComponent<Jugador>());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.GetComponent<Jugador>().SumarActivado = false;
            jugadoresDentro.Remove(collision.gameObject.GetComponent<Jugador>());
        }
    }

    private void Update()
    {
        if (jugadoresDentro.Count >= 2)
        {
            NoSumar();
            hill.color = Color.yellow;
        }
        else if (jugadoresDentro.Count == 1)
        {
            jugadoresDentro[0].SumarActivado = true;
            hill.color = jugadoresDentro[0].playerColor;
        }
        else hill.color = Color.white;
        foreach (Jugador jugador in jugadoresDentro)
        {
            if(!jugador.isActiveAndEnabled)
            {
                jugadoresDentro.Remove(jugador);
            }
        }

    }

    public void NoSumar()
    {
        Jugador[] jugadoresTemporal = FindObjectsOfType<Jugador>();
        foreach (Jugador player in jugadoresTemporal)
        {
            player.SumarActivado = false;
        }
    }

}
