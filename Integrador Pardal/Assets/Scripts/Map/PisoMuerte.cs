using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PisoMuerte : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !GameManager.MuerteSubita)
        {
            StartCoroutine(GameManager.Spawn(collision.gameObject));
        }

        else if (collision.CompareTag("Player") & GameManager.MuerteSubita)
        {
            GameManager.EliminarJugador(collision.gameObject);
          
        }
    }
}
